REVEAL_JS_URL:=https://github.com/hakimel/reveal.js/archive/master.tar.gz

RSCRIPT:=Rscript

BIBLIOGRAPHY_DIR:=bib
BIBLIOGRAPHY:=references.bib
MANUAL_BIBLIOGRAPHY:=manual_references.bib
PUBMED2BIBTEX:=helper/pubmed2bibtex.xsl
CITATION_SCRIPT:=helper/make_references.sh
CSL:=csl/presentation.csl

PRESENTATION:=md/slides.Rmd

clean:
	rm -rf public

build: clean build-citations 
	mkdir -p public/reveal/js && mkdir public/reveal/css && mkdir public/reveal/plugin && \
	curl -L -o public/reveal.tar.gz $(REVEAL_JS_URL) && \
	tar xzf public/reveal.tar.gz -C public && rm public/*.tar.gz && \
	mv public/reveal.js-*/dist/*.css public/reveal/css/ && \
	mv public/reveal.js-*/dist/theme public/reveal/css/ && \
	mv public/reveal.js-*/dist/*.js public/reveal/js/ && \
	mv public/reveal.js-*/plugin/* public/reveal/plugin/ && \
	rm -r public/reveal.js* && \
	$(RSCRIPT) -e "library(rmarkdown); render(\"md/slides.Rmd\")" && \
	pandoc -t revealjs -s -o public/index.html md/slides.md -V \
		revealjs-url="reveal" --metadata pagetitle="lecture" --css "custom.css" \
		--bibliography $(BIBLIOGRAPHY_DIR)/$(BIBLIOGRAPHY) --csl $(CSL) && \
	rm md/*.md && \
	cat css/* >> public/custom.css && \
	cp -r images public/images
	
build-citations:
	$(CITATION_SCRIPT) $(PRESENTATION) $(PUBMED2BIBTEX) $(BIBLIOGRAPHY_DIR)/$(BIBLIOGRAPHY) && \
	cat $(BIBLIOGRAPHY_DIR)/$(MANUAL_BIBLIOGRAPHY) >> $(BIBLIOGRAPHY_DIR)/$(BIBLIOGRAPHY) && \
	rm temp.xml

